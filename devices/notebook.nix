{ config, pkgs, lib, ... }:
{
  networking.hostName = "notebook";
  system.stateVersion = "23.05";

  users.users.user = {
    packages = with pkgs; [
      zoom-us
      webex
      element-desktop
      discord
      spotify
      xcape
      openfortivpn
    ];
  };


  # -------------------------------------------------------------------------- #
  # hardware configuration
  # -------------------------------------------------------------------------- #
  services.fstrim.enable = lib.mkDefault true;

  services.tlp = {
      enable = true;
      settings = {
          CPU_SCALING_GOVERNOR_ON_AC = "performance";
          CPU_ENERGY_PERF_POLICY_ON_AC = "performance";

          CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
          CPU_ENERGY_PERF_POLICY_ON_BAT = "powersave";
          WIFI_PWR_ON_BAT = "off";
      };
  };

  hardware.cpu.amd.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;

  # acpi
  boot = {
      extraModulePackages =  with config.boot.kernelPackages; [
        rtl8821ce
        acpi_call
      ];
      kernelModules = [
         "acpi_call"
         "rtl8821ce"
      ];
      kernelParams = [ "amd_pstate=active" ];
  };

  services.logind.extraConfig = ''
    HandlePowerKey=poweroff
    HandleLidSwitch=ignore
    HandleLidSwitchDocked=ignore
    HandleLidSwitchExternalPower=ignore
  '';
  # -------------------------------------------------------------------------- #
}
