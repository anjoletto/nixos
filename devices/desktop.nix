{ config, pkgs, lib, ... }:
{
  networking.hostName = "desktop";
  system.stateVersion = "24.05";

  users.users.user = {
    packages = with pkgs; [
      mplayer
      spotify
      nodejs
      procps
      luarocks
      beeper
      discord
      element-desktop
      webex
    ];
  };

  services.flatpak.enable = true;

  programs.steam.enable = true;

  # -------------------------------------------------------------------------- #
  # hardware configuration
  # -------------------------------------------------------------------------- #
  boot.kernelParams = ["intel_pstate=active"];

  hardware = {
    enableRedistributableFirmware = true;
    cpu.intel.updateMicrocode = true;

    graphics.extraPackages = with pkgs; [
      vaapiIntel
      libvdpau-va-gl
      intel-media-driver
    ];
  };

  boot.initrd.kernelModules = [ "i915" ];

  environment.variables = {
    VDPAU_DRIVER = lib.mkIf config.hardware.graphics.enable (lib.mkDefault "va_gl");
  };

  services = {
    fstrim.enable = lib.mkDefault true;
    thermald.enable = true;
  };

  services.tlp = {
      enable = true;
      settings = {
          CPU_SCALING_GOVERNOR_ON_AC = "performance";
          CPU_ENERGY_PERF_POLICY_ON_AC = "performance";
      };
  };
  # -------------------------------------------------------------------------- #
}

