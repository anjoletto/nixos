{ config, pkgs, ...}:
{
  # -------------------------------------------------------------------------- #
  # enable non-free stuff
  # -------------------------------------------------------------------------- #
  hardware.enableAllFirmware = true;
  nixpkgs.config.allowUnfree = true;
  # -------------------------------------------------------------------------- #

  # -------------------------------------------------------------------------- #
  # nix configuration
  # -------------------------------------------------------------------------- #
  nix.settings.auto-optimise-store = true;

  system.autoUpgrade = {
    enable = true;
    allowReboot = false;
    dates = "12:00";
    channel = "https://channels.nixos.org/nixos-24.05";
  };

  nix.gc = {
    automatic = true;
    options = "--delete-older-than 7d";
    dates = "12:30";
  };
  # -------------------------------------------------------------------------- #

  # -------------------------------------------------------------------------- #
  # set boot configuration
  # -------------------------------------------------------------------------- #
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  #boot.kernelPackages = pkgs.linuxPackages_zen;
  # -------------------------------------------------------------------------- #

  # -------------------------------------------------------------------------- #
  # other configurations
  # -------------------------------------------------------------------------- #
  zramSwap.enable = true;
  time.timeZone = "America/Sao_Paulo";
  powerManagement.enable = true;

  services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
  };

  i18n.defaultLocale = "en_US.UTF-8";
  console.keyMap = "emacs2";

  programs.light.enable = true;
  programs.zsh = {
    enable = true;
    enableCompletion = true;
  };

  systemd.extraConfig = "DefaultTimeoutStopSec=3s";
  # -------------------------------------------------------------------------- #

  # -------------------------------------------------------------------------- #
  # bluetooth
  # -------------------------------------------------------------------------- #
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  hardware.bluetooth.settings = {
    General = {
      Enable = "Source,Sink,Media,Socket";
    };
  };
  # -------------------------------------------------------------------------- #

  # -------------------------------------------------------------------------- #
  # network
  # -------------------------------------------------------------------------- #
  networking.networkmanager.enable = true;
  programs.nm-applet.enable = true;
  users.extraGroups.networkmanager.members = [ "user" ];
  # -------------------------------------------------------------------------- #


  # -------------------------------------------------------------------------- #
  # packages
  # -------------------------------------------------------------------------- #
  environment.systemPackages = with pkgs; [
    acpi
    usbutils
    haskellPackages.iwlib
    psutils
    lm_sensors
    neovim
    tmux
    git
    zsh-completions
    sysstat
    ripgrep
  ];
  # -------------------------------------------------------------------------- #
}

