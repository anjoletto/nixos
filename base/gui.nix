{ config, pkgs, ... }:
{
  environment.pathsToLink = ["/libexec"];
  programs.dconf.enable = true;
  services.dbus.enable = true;

  services.xserver = {
      enable = true;
      exportConfiguration = true;
      displayManager.lightdm.background = "/.bg";
  };

  xdg.portal = {
    enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    config.common.default = "*";
  };

  fonts = {
    fontDir.enable = true;
    enableGhostscriptFonts = true;
    packages = with pkgs; [ font-awesome ];
  };
}
