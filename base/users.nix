{ config, pkgs, ... }:
{
  users.users.user = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "video"];
    shell = pkgs.zsh ;
    packages = with pkgs; [
      wget
      playerctl
      pamixer
      magic-wormhole
      zoxide
      alacritty
      lua53Packages.lua
      tree-sitter
      xorg.xhost
      clang
      clang-tools
      eza
      fd
      htop
      lxqt.lxqt-policykit
      gtk3-x11
      i3lock
      rofi
      arandr
      flameshot
      firefox
      pavucontrol
      dunst
      networkmanager
      networkmanagerapplet
      xclip
      ripdrag
      bat
      parallel
      unzip
      fzf
      direnv
    ];
  };
}
