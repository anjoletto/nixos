{ config, pkgs, ... }:
{
  services.autorandr.enable = true;
  services = {
    xserver = {
      windowManager.i3.enable = true;
      xkb = {
        layout = "us";
        variant = "altgr-intl";
        options = "ctrl:nocaps";
      };
    };
    displayManager.defaultSession = "none+i3";
  };

  systemd.user.services."dunst" = {
    enable = true;
    wantedBy = [ "default.target" ];
    serviceConfig.Restart = "always";
    serviceConfig.ExecStart = "${pkgs.dunst}/bin/dunst";
  };

  services.libinput = {
    enable = true;
    mouse.middleEmulation = true;
    mouse.scrollButton = 8;
    mouse.scrollMethod = "button";
    touchpad.disableWhileTyping = true;
    touchpad.sendEventsMode = "disabled-on-external-mouse";
  };

}
