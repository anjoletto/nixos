{ config, pkgs, ...}:
{
  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;
  users.users.user.extraGroups = [ "libvirtd" ];
  networking.firewall.trustedInterfaces = [ "virbr0" ];
}
