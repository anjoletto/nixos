{ config, pkgs, ...}:
{
  virtualisation.incus.enable = true;
  users.extraGroups.incus-admin.members = [ "user" ];
  networking.nftables.enable = true;
  networking.firewall.trustedInterfaces = [ "incusbr0" ];
}
