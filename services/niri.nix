{ config, pkgs, lib, ... }:
{
  programs.niri.enable = true;
  environment.systemPackages = with pkgs; [
    fuzzel
    rofi-wayland
    wayland
    xwayland
    xdg-utils
    glib
    wl-clipboard
    wdisplays
    kanshi
    swaylock
    xwayland-satellite
    foot
  ];
}
