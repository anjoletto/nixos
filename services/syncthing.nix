{ config, pkgs, ...}:
{
  services.syncthing = {
    enable = true;
    group = "users";
    user = "user";
    dataDir = "/home/user/archive/syncthing";
    configDir = "/home/user/.config/syncthing_config";
  };

  networking.firewall = {
    allowedTCPPorts = [ 8384 22000 ];
    allowedUDPPorts = [ 22000 21027 ];
  };
}
