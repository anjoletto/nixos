{ config, pkgs, ... }:
{
  imports =
    [
      ./hardware-configuration.nix
      ./devices/desktop.nix
      ./base/system.nix
      ./base/gui.nix
      ./base/users.nix
      ./services/hosts.nix
      ./services/incus.nix
      ./services/i3.nix
      ./services/niri.nix
    ];
}

